package com.five;

import java.util.Iterator;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * Jan 24, 2021
 **/

public class ForLoopBox {
	
	public static void main(String[] args) {
		
		System.out.println("Squar");
		
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print("* ");
			}
			System.out.println("");
		}
		
		System.out.println("\nRectangle");
		
		for (int k = 0; k < 4; k++) {
			for (int l = 0; l < 6; l++) {
				System.out.print("* ");
			}
			System.out.println("");
		}
		
	}

}
